<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
//    return view('welcome');
    $customer = (object)[
        'id' => 1,
        'cust_name' => 'John',
        'summ_all_customers' => 2000,
        'primary_phone' => '0724069977',
        'email' => 'john@example.com',
        'income_statement_1' => 'income_statement_1',
        'income_statement_2' => 'income_statement_2',
        'income_statement_3' => 'income_statement_3',
    ];
    $msg = false;
    $institutedetails = [];
    $dir = 'rtl';
    $phone_indexes = ['072', '073', '074'];
    $default_index = $phone_indexes[0];
    $emu3_enabled = false;
    $income_statement_enabled = false;
    $list_accounts_enable = false;
    return view('example.EXAMPLE2',
        compact('customer', 'msg', 'institutedetails', 'dir', 'phone_indexes', 'default_index', 'emu3_enabled', 'income_statement_enabled', 'list_accounts_enable')
    );
});

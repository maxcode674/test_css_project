@extends('welcome')  <!-- 'main-v3' -->


@section('styles')
<style>

@media(min-width: 1201px) {
    .col-lg-3 {
        width: 13%;
    }
}

.portlet.box.green {
    background: none;
    position: relative;
    overflow: hidden;
    margin-bottom: 0;
}

.portlet.box.green {
    border-color: -moz-use-text-color #fff #fff;
}

/*.table-accounts {*/
    /*width:72%!important;*/
/*}*/

.form-group .pull-right{
    margin-top:10px;
}

@media screen and (min-width: 800px) {
    .col-md-3 {
        margin-left: -1.5%;
    }
}


/*.page-content-wrapper{*/
    /*max-height:748px;*/
/*}*/

</style>
<link rel="stylesheet" href="css/styles.min.css">
<link href="/css/sweet-alert.css" rel="stylesheet">
@endsection

@section('content')

    <div class="step__body">

    <div class="step-details">

        {!! Form::model($customer, array('url' => '/loans/personal-details', 'method' => 'post','class'=>'form-horizontal','id'=>'form-personal')) !!}

        @if(isset($customer))
        {!! Form::hidden('id', $customer->id) !!}
        @endif

        @if ($msg)
           <p>{{$msg}}</p>
        @endif
        <div class="step-details__row">
            <div class="step-details__column step-details__column--2-3">

                <div class="field field--location step-details__field js-error">
                    {!! Form::label('name', trans('Name'),['class'=>'field__label']) !!}
                    <div class="field__input-wrapper">
                        {!! Form::text('cust_name',  $customer->cust_name,['class'=>'field__input','readonly' => 'true','placeholder'=>trans('Name')]) !!}
                    </div>
                </div>
            </div>
            <div class="step-details__column step-details__column--1-3">
                <div class="field step-details__field <?php echo ($errors->has('Account_Balance'))?'has-error':''?> ">
                    {!! Form::label('Account_Balance', trans('סך כספים מנוהלים'),['class'=>'field__label']) !!}
                    <div class="field__input-wrapper">
                        {!! Form::text('Account_Balance',  number_format($customer->summ_all_customers),['class'=>'field__input','readonly' => 'true']) !!}
                        @if($errors->has('Account_Balance'))
                            <span class="help-block"> {{$errors->first('Account_Balance')}} </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
            <div class="form-group  ">
                <label for="street_address" class="col-lg-3 col-md-2 col-sm-2 col-xs-12 control-label"></label>
                <div class="col-md-4 col-sm-6 col-xs-8">
                    <span class="field__validation-msg"> <?php echo trans('note personal details') ?> </span>
                </div>
            </div>

            <?php /*
            <div class="form-group">
                {!! Form::label('name', trans('Date of Birth'),['class'=>'col-lg-3 col-md-2 col-sm-2 col-xs-12 control-label']) !!}
                <div class="col-md-4 col-sm-6 col-xs-8">
                    {!! Form::text('dateofbirth',  Input::old('dateofbirth'),['class'=>'form-control input-circle','readonly' => 'true','placeholder'=>trans('Name'),'id'=>'dateofbirth111']) !!}
                </div>
            </div>
            */
                ?>

            <?php if(!empty($institutedetails['google_api'])){ ?>

                <div class="form-group ">
                    {!! Form::label('google_address', trans('Google address'),['class'=>'col-lg-3 col-md-2 col-sm-2 col-xs-12 control-label']) !!}
                    <div class="col-md-4 col-sm-6 col-xs-8">
                        {!! Form::text('google_address',  Input::old('google_address'),['class'=>'form-control input-circle','id'=>'autocomplete', 'placeholder'=>trans('Address Client')]) !!}
                    </div>

                    <div >
                        <a type="button" onclick="insert_data_address()" class="btn btn-info fa fa-exchange" href="javascript:void(0);"> {{ trans('להכניס') }}</a>
                    </div>


                </div>

            <?php } ?>


            <input type="hidden" id="route" value="">
            <input type="hidden" id="street_number" value="">
            <input type="hidden" id="locality" value="">
            <input type="hidden" id="administrative_area_level_1" value="">
            <input type="hidden" id="postal_code" value="">
            <input type="hidden" id="country" value="">
            <input type="hidden" id="flag_submit" value="0">
        <div class="step-details__row">
            <div class="step-details__column step-details__column--1-3">
                <div class="field step-details__field <?php echo ($errors->has('street_address'))?'has-error':''?> ">
                    {!! Form::label('street_address', trans('Street'),['class'=>'field__label']) !!}
                    <div class="field__input-wrapper">
                        {!! Form::text('street_address',  Input::old('street_address'),['class'=>'field__input','readonly' => 'true','id'=>'route_f','placeholder'=>trans('Street')]) !!}
                        @if($errors->has('street_address'))
                        <span class="field__validation-msg"> {{$errors->first('street_address')}} </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="step-details__column step-details__column--1-3">
                <div class="field step-details__field <?php echo ($errors->has('house_num'))?'has-error':''?> ">
                    {!! Form::label('house_num', trans('House #'),['class'=>'field__label']) !!}
                    <div class="field__input-wrapper">
                        {!! Form::text('house_num',  Input::old('house_num'),['class'=>'field__input','readonly' => 'true','id'=>'street_number_f','placeholder'=>trans('House #')]) !!}
                        @if($errors->has('house_num'))
                        <span class="field__validation-msg"> {{$errors->first('house_num')}} </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="step-details__column step-details__column--1-3">
                <div class="field step-details__field <?php echo ($errors->has('city'))?'has-error':''?> ">
                    {!! Form::label('city', trans('City'),['class'=>'field__label']) !!}
                    <div class="field__input-wrapper">
                        {!! Form::text('city',  Input::old('city'),['class'=>'field__input','readonly' => 'true','id'=>'locality_f','placeholder'=>trans('City')]) !!}
                        @if($errors->has('city'))
                        <span class="field__validation-msg"> {{$errors->first('city')}} </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="step-details__row">
            <div class="step-details__column step-details__column--1-3">
                <div class="field step-details__field <?php echo ($errors->has('area'))?'has-error':''?> ">
                    {!! Form::label('area', trans('מחוז'),['class'=>'field__label']) !!}
                    <div class="field__input-wrapper">
                        {!! Form::text('area',  Input::old('area'),['class'=>'field__input','readonly' => 'true','id'=>'administrative_area_level_1_f','placeholder'=>trans('מחוז')]) !!}
                        @if($errors->has('area'))
                        <span class="help-block"> {{$errors->first('area')}} </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="step-details__column step-details__column--1-3">
                <div class="field step-details__field <?php echo ($errors->has('zip'))?'has-error':''?> ">
                    {!! Form::label('zip', trans('Zip'),['class'=>'field__label']) !!}
                    <div class="field__input-wrapper">
                        {!! Form::text('zip',  Input::old('zip'),['class'=>'field__input','id'=>'postal_code_f','placeholder'=>trans('Zip Code')]) !!}
                        @if($errors->has('zip'))
                        <span class="help-block"> {{$errors->first('zip')}} </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="step-details__column step-details__column--1-3">
                <div class="field step-details__field <?php echo ($errors->has('country'))?'has-error':''?> ">
                    {!! Form::label('country', trans('Country'),['class'=>'field__label']) !!}
                    <div class="field__input-wrapper">
                        {!! Form::text('country',  Input::old('country'),['class'=>'field__input','readonly' => 'true','id'=>'country_f','placeholder'=>trans('Country')]) !!}
                        @if($errors->has('country'))
                        <span class="help-block"> {{$errors->first('country')}} </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="step-details__row">
            <div class="step-details__column step-details__column--1-3">
                <div class="field field--phone step-details__field <?php echo ($errors->has('secondary_phone'))?'has-error':''?> ">
                    {!! Form::label('secondary_phone', trans('Home Phone'),['class'=>'field__label']) !!}
                    <div class="field__input-wrapper">
                        {!! Form::text('secondary_phone',  Input::old('secondary_phone'),['class'=>'field__input','placeholder'=>trans('Home Phone')]) !!}
                        @if($errors->has('secondary_phone'))
                        <span class="field__validation-msg"> {{$errors->first('secondary_phone')}} </span>
                        @endif
                    </div>
                </div>
            </div>
            <?php /*
            <div class="form-group">
                {!! Form::label('work_phone', trans('Work Phone'),['class'=>'col-lg-3 col-md-2 col-sm-2 col-xs-12 control-label']) !!}
                <div class="col-md-4 col-sm-6 col-xs-8">
                    {!! Form::text('work_phone',  Input::old('work_phone'),['class'=>'form-control input-circle','readonly' => 'true','placeholder'=>trans('Work Phone')]) !!}
                </div>
            </div>
            */ ?>

            @if($dir == 'rtl')
                <div class="step-details__column step-details__column--1-3">
                    <div class="field field--phone step-details__field">
                        {!! Form::label('primary_phone', trans('Cellular'),['class'=>'field__label']) !!}
                        <div class="field__wrapper">
                            <div class="field__input-wrapper <?php echo ($errors->has('primary_phone'))?'has-error':''?>">
                                {!! Form::text('primary_phone',  $customer->primary_phone,['class'=>'field__input','placeholder'=>trans('Cellular')]) !!}
                                @if($errors->has('primary_phone'))
                                    <span class="field__validation-msg"> {{$errors->first('primary_phone')}} </span>
                                @endif
                            </div>

                            <div class="field__select-wrapper">
                                <svg class="icon field__select-arrow">
                                    <use xlink:href="#icon-arrow-down"></use>
                                </svg>
                                {!! Form::select('primary_phone_index', $phone_indexes, $default_index, ['class'=>'field__select']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            @if($dir == 'ltr')
                <div class="step-details__column step-details__column--1-3">
                    <div class="field field--phone step-details__field">
                        {!! Form::label('primary_phone', trans('Cellular'),['class'=>'field__label']) !!}
                        <div class="field__wrapper">
                            <div class="field__select-wrapper">
                                <svg class="icon field__select-arrow">
                                    <use xlink:href="#icon-arrow-down"></use>
                                </svg>
                                {!! Form::select('primary_phone_index', $phone_indexes, $default_index, ['class'=>'field__select']) !!}
                            </div>

                            <div class="field__input-wrapper<?php echo ($errors->has('primary_phone'))?'has-error':''?>">
                                {!! Form::text('primary_phone',  $customer->primary_phone,['class'=>'field__input','placeholder'=>trans('Cellular')]) !!}
                                @if($errors->has('primary_phone'))
                                    <span class="field__validation-msg"> {{$errors->first('primary_phone')}} </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="step-details__column step-details__column--1-3">
                <div class="field step-details__field <?php echo ($errors->has('email'))?'has-error':''?> ">
                    {!! Form::label('name', trans('Email'),['class'=>'field__label']) !!}
                    <div class="field__input-wrapper">
                        {!! Form::text('email',  $customer->email,['class'=>'field__input','placeholder'=>trans('Email Address')]) !!}
                        @if($errors->has('email'))
                            <span class="field__validation-msg"> {{$errors->first('email')}} </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
            @if($emu3_enabled)
                <div class="form-group flagcustomerinformed <?php echo ($errors->has('FlagCustomerInformed'))?'has-error':''?> ">
                    <div class="form-group">
                        <label for="FlagCustomerInformed" class="col-lg-3 col-md-2 col-sm-2 col-xs-12 control-label"></label>
                        <div class="col-md-4 col-sm-6 col-xs-8">
                            <span class="help-block"> <?php echo trans('FlagCustomerInformed details') ?> </span>
                        </div>
                    </div>
                </div>
            @endif

            @if($income_statement_enabled)
                <div class="form-group <?php echo ($errors->has('income_statement_1'))?'has-error':''?> ">
                    {!! Form::label('name', trans('income_statement_1'),['class'=>'col-lg-3 col-md-2 col-sm-2 col-xs-12 control-label']) !!}
                    <div class="col-md-4 col-sm-6 col-xs-8">
                        {!! Form::text('income_statement_1',  $customer->income_statement_1,['class'=>'form-control input-circle','placeholder'=>trans('Placeholder_income_statement_1')]) !!}
                        @if($errors->has('income_statement_1'))
                            <span class="help-block"> {{$errors->first('income_statement_1')}} </span>
                        @endif
                    </div>
                </div>

                <div class="form-group <?php echo ($errors->has('income_statement_2'))?'has-error':''?> ">
                    {!! Form::label('name', trans('income_statement_2'),['class'=>'col-lg-3 col-md-2 col-sm-2 col-xs-12 control-label']) !!}
                    <div class="col-md-4 col-sm-6 col-xs-8">
                        {!! Form::text('income_statement_2',  $customer->income_statement_2,['class'=>'form-control input-circle','placeholder'=>trans('Placeholder_income_statement_2')]) !!}
                        @if($errors->has('income_statement_2'))
                            <span class="help-block"> {{$errors->first('income_statement_2')}} </span>
                        @endif
                    </div>
                </div>

                <div class="form-group <?php echo ($errors->has('income_statement_3'))?'has-error':''?> ">
                    {!! Form::label('name', trans('income_statement_3'),['class'=>'col-lg-3 col-md-2 col-sm-2 col-xs-12 control-label']) !!}
                    <div class="col-md-4 col-sm-6 col-xs-8">
                        {!! Form::text('income_statement_3',  $customer->income_statement_3,['class'=>'form-control input-circle','placeholder'=>trans('Placeholder_income_statement_3')]) !!}
                        @if($errors->has('income_statement_3'))
                            <span class="help-block"> {{$errors->first('income_statement_3')}} </span>
                        @endif
                    </div>
                </div>

                <div class="form-group <?php echo ($errors->has('check_income_statement'))?'has-error':''?> ">
                    {!! Form::label('check_income_statement', trans('check_income_state'),['class'=>'col-lg-3 col-md-2 col-sm-2 col-xs-12 control-label']) !!}
                    <div class="col-md-4 col-sm-6 col-xs-8">

                        {!! Form::checkbox('check_income_statement', '1', false) !!}

                        @if($errors->has('check_income_statement'))
                            <span class="help-block"> {{$errors->first('check_income_statement')}} </span>
                        @endif
                    </div>
                </div>
            @endif

            @if($list_accounts_enable)
            <div class="form-group">
                {!! Form::label('request_type', trans('אני רוצה'),['class'=>'col-lg-3 col-md-2 col-xs-12 col-sm-2 control-label']) !!}
                <div class="col-md-4 col-sm-6 col-xs-5">

                    @if(in_array(\Config::get('constants.NEW_LOAN'), $request_type))
                        <div>
                        {!! Form::radio('request_type', \Config::get('constants.NEW_LOAN'), true, array('id'=>'request_type_1')) !!}
                        {!! Form::label('request_type_1', trans('לבקש הלוואה חדשה'),['class'=>'control-label']) !!}
                        </div>
                    @endif

                    @if(in_array(\Config::get('constants.RECYCLING_NEW_LOAN'), $request_type))
                        <div>
                        {!! Form::radio('request_type', \Config::get('constants.RECYCLING_NEW_LOAN'), false, array('id'=>'request_type_2')) !!}
                        {!! Form::label('request_type_2', trans('לבקש מיחזור של הלוואה קיימת וחדש'),['class'=>'control-label']) !!}
                        </div>
                    @endif

                    @if(in_array(\Config::get('constants.RECYCLING_LOAN'), $request_type))
                        <div>
                            {!! Form::radio('request_type', \Config::get('constants.RECYCLING_LOAN'), false, array('id'=>'request_type_3')) !!}
                            {!! Form::label('request_type_3', trans('לבקש פירעון הלוואה קיימת'),['class'=>'control-label']) !!}
                        </div>
                    @endif

                    @if(in_array(\Config::get('constants.REPAYMENT_LOAN'), $request_type))
                        <div>
                            {!! Form::radio('request_type', \Config::get('constants.REPAYMENT_LOAN'), false, array('id'=>'request_type_4')) !!}
                            {!! Form::label('request_type_4', trans('לבקש מיחזור של הלוואה קיימת'),['class'=>'control-label']) !!}
                        </div>
                    @endif

                        @if(in_array(\Config::get('constants.REPAYMENT_WITHDRAWAL_LOAN'), $request_type))
                            <div>
                                {!! Form::radio('request_type', \Config::get('constants.REPAYMENT_WITHDRAWAL_LOAN'), false, array('id'=>'request_type_5')) !!}
                                {!! Form::label('request_type_5', trans('לבקש פירעון הלוואה קיימת וחדש'),['class'=>'control-label']) !!}
                            </div>
                        @endif

                </div>
            </div>
            @endif

            @if($list_accounts_enable)

            @if($errors->has('select_accounts'))
                <span  class="help-block has-error" style="color:red;"> {{$errors->first('select_accounts')}}  </span>
            @endif

                <table id="table-list-account" dir="{{$dir}}" class="table-accounts table table-striped  table-bordered">
                <tfoot>
                <tr>
                    <td>{{ trans('סהייכ למיחזור או פירעון') }}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td> <span id="table_principal_balance">{{number_format($sum_principal_balance)}}</span> </td>
                    <td> <span id="table_interest_balance">{{number_format($sum_interest_balance)}}</span> </td>
                    <td> <span id="table_total_debt_balance">{{number_format($sum_total_debt_balance)}}</span> </td>
                    <td> <span id="table_total_balance">{{number_format($sum_total_balance)}}</span> </td>
                    <td></td>
                </tr>
                </tfoot>
                <tbody>
                <tr>
                    <th style="text-align: center; font-weight: bold;">{{ trans('מספר חשבון בנק') }}</th>
                    <th style="text-align: center; font-weight: bold;">{{ trans('קוד מייה') }}</th>
                    <th style="text-align: center; font-weight: bold;">{{ trans('שם קופה') }}</th>
                    <th style="text-align: center; font-weight: bold;">{{ trans('סוג הלוואה') }}</th>
                    <th style="text-align: center; font-weight: bold;">{{ trans('% ריבית') }}</th>

                    <th style="text-align: center; font-weight: bold;">{{ trans('principal_revaluation') }}</th>
                    <th style="text-align: center; font-weight: bold;">{{ trans('interest_balance') }}</th>
                    <th style="text-align: center; font-weight: bold;">{{ trans('יתרת חוב') }}</th>

                    <th style="text-align: center; font-weight: bold;">{{ trans('יתרת הלוואה בלתי מסולקת') }}</th>
                    <th style="text-align: center; font-weight: bold;">{{ trans('מסי תשלומים שנותרו') }}</th>
                    <th style="text-align: center; font-weight: bold;">{{ trans('תאריך סיום הלוואה') }}</th>
                    <th style="text-align: center; font-weight: bold;">{{ trans('סמן למיחזור או פירעון') }}
                        <br>
                        <label><input type="checkbox" id="select_all" value="1" />{{ trans('בחר הכל') }}</label>
                        <label><input type="checkbox" id="select_none" value="1" />{{ trans('נקה הכל') }}</label>
                    </th>
                </tr>
                @foreach($list_accounts as $account)
                <tr>
                    <td>{{$account->account_number}}</td>
                    <td>{{$account->fund_id}}</td>
                    <td>{{$account->fund_name}}</td>
                    <td>{{$account->loan_type}}</td>
                    <td>{{$account->float_interest_percent}}</td>

                    <td>{{($account->principal_revaluation == 0)?'':number_format($account->principal_revaluation)}}</td>
                    <td>{{($account->interest_balance == 0)?'':number_format($account->interest_balance)}}</td>
                    <td>{{($account->total_debt_balance == 0)?'':number_format($account->total_debt_balance)}}</td>

                    <td>{{($account->principal_revaluation + $account->interest_balance + $account->total_debt_balance == 0)?'':number_format($account->principal_revaluation + $account->interest_balance + $account->total_debt_balance)}}</td>

                    <td>{{$account->interest_payments_remaining}}</td>
                    <td>{{(!empty($account->date_completion))?(string)date('d/m/Y',strtotime($account->date_completion)):''}}</td>
                    <td>
                        <input type="checkbox" class= "rowchk" name="piraon_select[]"
                               totalbalance="{{$account->principal_revaluation + $account->interest_balance + $account->total_debt_balance}}"
                               principalbalance="{{$account->principal_revaluation}}"
                               interestbalance="{{$account->interest_balance}}"
                               totaldebtbalance="{{$account->total_debt_balance}}"
                               value="{{$account->unique_id}}" />
                    </td>
                </tr>
                @endforeach
                </tbody>
                </table>


                <table id="table-list-account-withdrawal" dir="{{$dir}}" class="table-accounts table table-striped  table-bordered">
                    <tfoot>
                    <tr>
                        <td>{{ trans('סהייכ למיחזור או פירעון') }}</td>
                        <td></td>
                        <td></td>
                        <td> <span id="table_withdrawal_balance_withdrawal">{{$sum_withdrawal_balance_withdrawal}}</span> </td>
                        <td> <span id="table_principal_balance_withdrawal">{{$sum_principal_balance_withdrawal}}</span> </td>
                        <td> <span id="table_interest_balance_withdrawal">{{$sum_interest_balance_withdrawal}}</span> </td>
                        <td> <span id="table_total_debt_balance_withdrawal">{{$sum_total_debt_balance_withdrawal}}</span> </td>
                        <td> <span id="table_total_balance_withdrawal">{{$sum_total_balance_withdrawal}}</span> </td>
                        <td></td>
                    </tr>
                    </tfoot>
                    <tbody>
                    <tr>
                        <th style="text-align: center; font-weight: bold;">{{ trans('מספר חשבון בנק') }}</th>
                        <th style="text-align: center; font-weight: bold;">{{ trans('קוד מייה') }}</th>
                        <th style="text-align: center; font-weight: bold;">{{ trans('שם קופה') }}</th>
                        <th style="text-align: center; font-weight: bold;">{{ trans('סכום משיכה') }}</th>
                        <th style="text-align: center; font-weight: bold;">{{ trans('principal_revaluation') }}</th>
                        <th style="text-align: center; font-weight: bold;">{{ trans('interest_balance') }}</th>
                        <th style="text-align: center; font-weight: bold;">{{ trans('יתרת חוב') }}</th>
                        <th style="text-align: center; font-weight: bold;">{{ trans('יתרת הלוואה בלתי מסולקת') }}</th>

                        <th style="text-align: center; font-weight: bold;">{{ trans('סמן למיחזור או פירעון') }}
                            <br>
                            <label><input type="checkbox" id="select_all-withdrawal" value="1" />{{ trans('בחר הכל') }}</label>
                            <label><input type="checkbox" id="select_none-withdrawal" value="1" />{{ trans('נקה הכל') }}</label>
                        </th>
                    </tr>
                    @foreach($list_accounts_withdrawal as $account)
                        <tr>
                            <td>{{$account->account_number}}</td>
                            <td>{{$account->fund_id}}</td>
                            <td>{{$account->fund_name}}</td>

                            <td class="sum-withdrawal">{{$account->principal_revaluation}}</td> <!-- !!!! -->

                            <td>{{($account->principal_revaluation == 0)?'':number_format($account->principal_revaluation)}}</td>
                            <td>{{($account->interest_balance == 0)?'':number_format($account->interest_balance)}}</td>
                            <td>{{($account->total_debt_balance == 0)?'':number_format($account->total_debt_balance)}}</td>
                            <td>{{($account->principal_revaluation + $account->interest_balance + $account->total_debt_balance == 0)?'':number_format($account->principal_revaluation + $account->interest_balance + $account->total_debt_balance)}}</td>

                            <td>
                                <input type="checkbox" class= "rowchk-withdrawal" name="withdrawal_select[]"
                                       totalbalance="{{$account->principal_revaluation + $account->interest_balance}}"
                                       principalbalance="{{$account->principal_revaluation}}"
                                       interestbalance="{{$account->interest_balance}}"
                                       withdrawalbalance="{{$account->principal_revaluation + $account->interest_balance + $account->total_debt_balance}}"
                                       withdrawaltotaldebtbalance="{{$account->total_debt_balance}}"
                                       value="{{$account->unique_id}}" />

                                <select class= "select-withdrawal" name="withdrawal_percent[]">
                                    @foreach($list_selects_withdrawal as $value=>$name)
                                        <option value="{{$account->unique_id}}-{{$value}}">{{$name}}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif

            <?php /*
            <div class="form-group <?php echo ($errors->has('monthly_income'))?'has-error':''?> ">
                {!! Form::label('monthly_income', trans('Data entry'),['class'=>'col-lg-3 col-md-2 col-sm-2 col-xs-12 control-label']) !!}
                <div class="col-md-4 col-sm-6 col-xs-8">
                    {!! Form::text('monthly_income',  $customer->Monthly_income,['class'=>'form-control input-circle','placeholder'=>trans('example 7500')]) !!}
                    @if($errors->has('monthly_income'))
                    <span class="help-block"> {{$errors->first('monthly_income')}} </span>
                    @endif
                </div>
            </div>
            */?>

            <?php /*
            <div class="form-group">
                {!! Form::label('employer_name', trans('Employer Name'),['class'=>'col-lg-3 col-md-2 col-sm-2 col-xs-12 control-label']) !!}
                <div class="col-md-4 col-sm-6 col-xs-8">
                    {!! Form::text('employer_name',  Input::old('employer_name'),['class'=>'form-control input-circle','readonly' => 'true','placeholder'=>trans('Employer')]) !!}
                </div>
            </div>
               */?>

            <div class="col-md-12">
                <div class="form-group pull-right">
                    <a type="button" class="btn btn-danger fa fa-remove" href="/"> {{ trans('לוגאאוט') }}</a>
                    <button type="submit" class="btn btn-success fa fa-angle-double-left"> {{ trans('Save And Continue Amit Step 2') }}</button>
                </div>
            </div>

        </div>


        {!! Form::close() !!}
    </div>
</div>

<script src="/loancalc/js/sweetalert.min.js"></script>
<script>

    function sum_of_total_balance(){
        var summ_total_balance = 0;
        var summ_principal_balance = 0;
        var summ_interest_balance = 0;
        var summ_total_debt_balance = 0;
        $("input:checkbox[class=rowchk]").each(function () {
            if(this.checked){
                var value = $(this).val();
                var sum_item1 = $(this).attr('totalbalance');
                summ_total_balance = +summ_total_balance + +sum_item1;
                summ_total_balance = Math.round(summ_total_balance);

                var sum_item2 = $(this).attr('principalbalance');
                summ_principal_balance = +summ_principal_balance + +sum_item2;
                summ_principal_balance = Math.round(summ_principal_balance);

                var sum_item3 = $(this).attr('interestbalance');
                summ_interest_balance = +summ_interest_balance + +sum_item3;
                summ_interest_balance = Math.round(summ_interest_balance);

                var sum_item4 = $(this).attr('totaldebtbalance');
                summ_total_debt_balance = +summ_total_debt_balance + +sum_item4;
                summ_total_debt_balance = Math.round(summ_total_debt_balance);
            }
        });

        //$('#table_sum_balance').text(summ.toFixed(0));
        $('#table_total_balance').text(numberWithCommas(summ_total_balance));
        $('#table_principal_balance').text(numberWithCommas(summ_principal_balance));
        $('#table_interest_balance').text(numberWithCommas(summ_interest_balance));
        $('#table_total_debt_balance').text(numberWithCommas(summ_total_debt_balance));
    }

    function sum_of_total_balance_withdrawal(){
        var summ_total_balance = 0;
        var summ_principal_balance = 0;
        var summ_interest_balance = 0;
        var summ_withdrawal = 0;
        var summ_total_debt_balance = 0;
        $("input:checkbox[class=rowchk-withdrawal]").each(function () {
            if(this.checked){
                var value = $(this).val();

                var sum_item5 = $(this).attr('withdrawaltotaldebtbalance');
                summ_total_debt_balance = +summ_total_debt_balance + +sum_item5;
                summ_total_debt_balance = Math.round(summ_total_debt_balance);

                var sum_item1 = $(this).attr('totalbalance');
                summ_total_balance = +summ_total_balance + +sum_item1 + + sum_item5;
                summ_total_balance = Math.round(summ_total_balance);

                var sum_item2 = $(this).attr('principalbalance');
                summ_principal_balance = +summ_principal_balance + +sum_item2;
                summ_principal_balance = Math.round(summ_principal_balance);

                var sum_item3 = $(this).attr('interestbalance');
                summ_interest_balance = +summ_interest_balance + +sum_item3;
                summ_interest_balance = Math.round(summ_interest_balance);

                var select_val =$(this).parent().find('select').val().split('-');
                var sum_item4 = $(this).attr('withdrawalbalance') * select_val[1] / 100;
                summ_withdrawal = +summ_withdrawal + +sum_item4;
                summ_withdrawal = Math.round(summ_withdrawal);
            }
        });

        //$('#table_sum_balance_withdrawal').text(summ.toFixed(0));
        $('#table_total_balance_withdrawal').text(numberWithCommas(summ_total_balance));
        $('#table_principal_balance_withdrawal').text(numberWithCommas(summ_principal_balance));
        $('#table_interest_balance_withdrawal').text(numberWithCommas(summ_interest_balance));
        $('#table_withdrawal_balance_withdrawal').text(numberWithCommas(summ_withdrawal));
        $('#table_total_debt_balance_withdrawal').text(numberWithCommas(summ_total_debt_balance));
    }

    function calculate_selects_withdrawal(){

        $(".select-withdrawal").each(function () {
            var select_val = this.value.split('-');
            var withdrawal =$(this).parent().find('input').attr('withdrawalbalance');
            var result = withdrawal * select_val[1] / 100;
            result = Math.round(result);

            $(this).parent().parent().find('.sum-withdrawal').text(numberWithCommas(result));

            sum_of_total_balance_withdrawal();
        });

    }

    $(document).ready(function() {

        //Default
        $("#select_all").prop('checked', true);
        $("input:checkbox[class=rowchk]").prop('checked', true);

        $("input:checkbox[class=rowchk]").change(function() {
            sum_of_total_balance();
        });

        $("#select_all").click( function() {
            if($(this).is(":checked")){
                $("input:checkbox[class=rowchk]").prop('checked', this.checked);
                $("#select_none").prop('checked', false);
                sum_of_total_balance();
            }else{
                $("input:checkbox[class=rowchk]").prop('checked', true);
                $("#select_none").prop('checked', false);
                $("#select_all").prop('checked', true);
                sum_of_total_balance();
            }
        });

        $("#select_none").click( function() {
            if($(this).is(":checked")){
                $("input:checkbox[class=rowchk]").prop('checked', false);
                $("#select_all").prop('checked', false);
                sum_of_total_balance();
            }else{
                $("input:checkbox[class=rowchk]").prop('checked', false);
                $("#select_all").prop('checked', false);
                $("#select_none").prop('checked', true);
                sum_of_total_balance();
            }
        });

        //withdrawal
        //Default
        $("#select_all-withdrawal").prop('checked', true);
        $("input:checkbox[class=rowchk-withdrawal]").prop('checked', true);
        calculate_selects_withdrawal();

        $("input:checkbox[class=rowchk-withdrawal]").change(function() {
            sum_of_total_balance_withdrawal();
        });

        $( ".select-withdrawal" ).change(function() {
            calculate_selects_withdrawal();
        });

        $("#select_all-withdrawal").click( function() {
            if($(this).is(":checked")){
                $("input:checkbox[class=rowchk-withdrawal]").prop('checked', this.checked);
                $("#select_none-withdrawal").prop('checked', false);
                sum_of_total_balance_withdrawal();
            }else{
                $("input:checkbox[class=rowchk-withdrawal]").prop('checked', true);
                $("#select_none-withdrawal").prop('checked', false);
                $("#select_all-withdrawal").prop('checked', true);
                sum_of_total_balance_withdrawal();
            }
        });

        $("#select_none-withdrawal").click( function() {
            if($(this).is(":checked")){
                $("input:checkbox[class=rowchk-withdrawal]").prop('checked', false);
                $("#select_all-withdrawal").prop('checked', false);
                sum_of_total_balance_withdrawal();
            }else{
                $("input:checkbox[class=rowchk-withdrawal]").prop('checked', false);
                $("#select_all-withdrawal").prop('checked', false);
                $("#select_none-withdrawal").prop('checked', true);
                sum_of_total_balance_withdrawal();
            }
        });

        //////////////////////////////////////////////////////////////////////////////////////////////

        if($('input[name=request_type]:checked').val() == {{\Config::get('constants.NEW_LOAN')}}){
            $('#table-list-account').hide();
            $('#table-list-account-withdrawal').hide();
        }else {
            if ($('input[name=request_type]:checked').val() == {{\Config::get('constants.REPAYMENT_WITHDRAWAL_LOAN')}}) {
                $('#table-list-account').hide();
                $('#table-list-account-withdrawal').show();
            } else {
                $('#table-list-account').show();
                $('#table-list-account-withdrawal').hide();
            }
        }


        //Start page
        var num = $('input[type=radio][name=request_type]:checked').val();
        if(num == {{\Config::get('constants.REPAYMENT_LOAN')}} || num == {{\Config::get('constants.REPAYMENT_WITHDRAWAL_LOAN')}}){
            $('.flagcustomerinformed').hide();
        }

        $('input[type=radio][name=request_type]').change(function() {
            if (this.value == {{\Config::get('constants.NEW_LOAN')}} || this.value == {{\Config::get('constants.RECYCLING_NEW_LOAN')}}
                || this.value == {{\Config::get('constants.RECYCLING_LOAN')}}) {
                $('.flagcustomerinformed').show();
            }else{
                $('.flagcustomerinformed').hide();
            }

            if (this.value == {{\Config::get('constants.NEW_LOAN')}}) {
                $('#table-list-account').hide();
                $('#table-list-account-withdrawal').hide();
            }else{
                if(this.value == {{\Config::get('constants.REPAYMENT_WITHDRAWAL_LOAN')}}){
                    $('#table-list-account-withdrawal').show();
                    $('#table-list-account').hide();
                }else{
                    $('#table-list-account-withdrawal').hide();
                    $('#table-list-account').show();
                }
            }
        });

        $('form').submit(function(event){

            if(validate()){

                var request_type = $('input[name = "request_type"]:checked').val();

                if(request_type == {{\Config::get('constants.NEW_LOAN')}} || request_type == {{\Config::get('constants.RECYCLING_NEW_LOAN')}} || request_type == undefined) {
                    swal({
                        title: "{{trans("calculates_eligibility_loan")}}",
                        text: "<?php echo trans("please_wait_few_seconds");?>",
                        imageUrl: "../gif/snake.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });

                    return true;
                }


                if(request_type == {{\Config::get('constants.RECYCLING_LOAN')}}){
                    swal({
                        title: "{{trans("calculates_eligibility_loan")}}",
                        text: "<?php echo trans("please_wait_few_seconds");?>",
                        imageUrl: "../gif/snake.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });

                    var flag_recycle = $('#flag_submit').val();

                    //Send ajax
                    if(flag_recycle == 0) {

                        var a = [];
                        $('input[name="piraon_select[]"]:checked').each(function(e) {
                            a.push(this.value);
                        });

                        $.post("/loans/ajax/checkRecyclingDetails", {checked_accounts: a})
                            .done(function (data) {

                                if (data.result == 1) {
                                    $('#flag_submit').val(1);
                                    jQuery('#form-personal').submit();
                                } else {
                                    recyclingLoanError(data.title, data.text);
                                }
                            });

                        return false;
                    }
                }

                if(request_type == {{\Config::get('constants.REPAYMENT_LOAN')}} || request_type == {{\Config::get('constants.REPAYMENT_WITHDRAWAL_LOAN')}}) {
                    var flag = $('#flag_submit').val();
                    if(flag == 0) {
                        event.preventDefault();
                        //jQuery('#form-personal').submit();

                        swal({
                                title: "{{trans("prepayment_title")}}",
                                text: "{{trans("prepayment_text")}}",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "{{trans("prepayment_confirm")}}",
                                cancelButtonText: "{{trans("prepayment_cancel")}}",
                                closeOnConfirm: true,
                                closeOnCancel: true
                            },
                            function (isConfirm) {
                                if (isConfirm) {

                                    $('#flag_submit').val(1);
                                    jQuery('#form-personal').submit();

                                } else {

                                }
                            });
                    }
                }
            }

        });
     });


    function validate(){

        var break_op = false;
        var zip = $("input[name=zip]").val();
        var email = $("input[name=email]").val();
        var secondary_phone = $("input[name=secondary_phone]").val();
        var primary_phone = $("input[name=primary_phone]").val();
        var account_balance = $("input[name=Account_Balance]").val();

        //Zip
        if(zip.length == 0) break_op = true;
        if(!$.isNumeric(zip)) break_op = true;

        //Email
        if(!isValidEmailAddress(email) && email.length != 0) break_op = true;

        //secondary_phone
        if(secondary_phone.length == 0 || secondary_phone.length < 9) break_op = true;

        //primary_phone
        if(primary_phone.length != 7 || !$.isNumeric(primary_phone)) break_op = true;

        //Account_Balance
        if(account_balance.length == 0) break_op = true;

        if(break_op){
            return false;
        }
        return true;
    }

    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        return pattern.test(emailAddress);
    }

    function recyclingLoanError(title, text){
        swal({
            title: title,
            text: text,
            html: true
        });

    }
</script>

    <?php if(!empty($institutedetails['google_api'])){ ?>
    <script>

      window.onload = function(){
        $('.pac-container.pac-logo').removeClass('pac-logo');
      }

      function insert_data_address(){

          if($('#autocomplete').val() != ''){
              $('#route_f').val($('#route').val());
              $('#street_number_f').val($('#street_number').val());
              $('#locality_f').val($('#locality').val());
              $('#administrative_area_level_1_f').val($('#administrative_area_level_1').val());
              $('#postal_code_f').val($('#postal_code').val());
              $('#country_f').val($('#country').val());
          }
      }

      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
//        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }

      function numberWithCommas(x) {
          return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $institutedetails['google_api']; ?>&libraries=places&callback=initAutocomplete&language=he"
        async defer></script>

    <?php } ?>

@endsection

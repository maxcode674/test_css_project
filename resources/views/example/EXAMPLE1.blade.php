@extends('main-v3')
@section('content')
<?php
ini_set('display_errors', 0);
include('../public/loancalc/functions.php');
?>

<link href="/loancalc/css/bootstrap.css" rel="stylesheet">
<link href="/loancalc/css/nouislider.css?v=2" rel="stylesheet">
<link href="/loancalc/css/nouislider.pips.css?v=2" rel="stylesheet">
<link href="/loancalc/css/nouislider.tooltips.css" rel="stylesheet">
<!-- <link rel="stylesheet" href="css/vanilla-cream-css/vanilla-cream.css"> -->
<link href="/css/app.css" rel="stylesheet">

<!--[if lt IE 9]>
<script src="js/html5shiv/dist/html5shiv.js"></script>
<script src="js/respond.js/dest/respond.min.js"></script>
<![endif]-->
<!-- Font Awesome CSS -->
<link rel="stylesheet" href="/loancalc/components/fontawesome/css/font-awesome.min.css">
<!-- App CSS -->
<link rel="stylesheet" href="/loancalc/css/mvpready-admin.css">

<link rel="stylesheet" href="/loancalc/css/custom.css">
<link rel="stylesheet" href="/css/custom-client.css">
<style>
    /*@media (min-width:992px){.container{width:1170px}}*/
    @media (min-width:1200px){.container{width:1170px}}
    h3{font-weight: 600;}
    #slider_interest{margin-left: 30px; margin-right: 20px;}
    .tsub_titlet{font-size: 17px; font-weight: bold;}
    .tsub_title{font-size: 17px;}
    .tvalue{font-size: 17px; font-weight: normal;}
    .pt_on{color: #50c2ec;}
    .pt_off{color: #111111;}
    #sButt1{color: #337ab7; text-decoration: underline;}
    #sButt2{color: #337ab7; text-decoration: underline;}
    #sButt3{color: #337ab7; text-decoration: underline;}
    .borderltab{border: 1px solid #F2F2F2;}
    .mouse-o-c:hover{background-color: transparent; -webkit-box-shadow: inset 0 0 80px #D6EAF9; box-shadow: inset 0 0 80px #D6EAF9;}
    .mouse-o-c:hover h2{background-color: transparent;}
    .mouse-o-c h2{background-color: #EDF6FC;}
    #mix_fixed{font-size: 17px;}
    #mix_prime{font-size: 17px;}

    .readMore .tooltip-inner {
        display: block;
        color: #727272;
        max-width: 750px;
        width: 100%;
        font-size: 14px;
        background: #fff;
        border: 1px #ccc solid;
        padding: 10px;
        -webkit-border-radius: 10px;
        -moz-border-radius: 10px;
        border-radius: 10px;
        z-index: 100000;
        margin-right: 15px;
        text-align: right;
    }

    .readMore .tooltip-arrow {
        display: none;
    }

    .tooltip.in {
        opacity: 1;
        filter: alpha(opacity=100);
    }


</style>
<script>
    var g_operation_fee = <?=number_format($userAccounts->get(0)->operation_fee,2)?>;
</script>

    <div class="col-sm-12">
        <form id="amorization-form" role="form">
        <input type="hidden" name="inst_id" id="inst_id" value="1">

        @include('elements.setup-block-loan-type')

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="graph_box" style="display:none;">
                    <div id="graphholder" class="demo-placeholder"></div>
                </div>
            </div>
        </div>
        <!-- end of vanilla cream -->
        <div class="row"> <!-- section for the graph -->
        </div>
        <!-- end div class row -->
        </form>
    </div><!-- end div class row -->

    <form id="act_form" method="post" action="/loans/save_loan">
        <input type="hidden" id="p_type" name="type" value=""/>
        <input type="hidden" id="p_interest" name="interest" value=""/>
        <input type="hidden" id="p_interest_type" name="interest_type" value=""/>
        <input type="hidden" id="p_start_month" name="start_month" value=""/>
        <input type="hidden" id="p_start_year" name="start_year" value=""/>
        <input type="hidden" id="p_loan_amount" name="loan_amount" value=""/>
        <input type="hidden" id="p_total_interestpaid" name="total_interestpaid" value=""/>
        <input type="hidden" id="p_monthly_payment" name="monthly_payment" value=""/>
        <input type="hidden" id="p_total_payment" name="total_payment" value=""/>
        <input type="hidden" id="p_years" name="years" value=""/>
        <input type="hidden" id="p_interest_float_percent" name="interest_float_percent" value=""/>
        <input type="hidden" id="p_interest_mix_percent" name="interest_mix_percent" value=""/>
    </form>
    <div class="row">
        <div class="col-md-12">
            <h3>
                {{trans('יתרת זכאותי להלוואה')}}
            </h3>
            <table class="table table-striped  table-bordered">
                <tr>
                    <th class="text-right" style="font-weight: bold;">{{trans("account bank setup")}}</th>
                    <th class="text-right" style="font-weight: bold;">{{trans('קוד מ"ה')}}</th>
                    <th class="text-right" style="font-weight: bold;">{{trans("שם קופה")}}</th>
                    <th class="text-right" style="font-weight: bold;">{{trans("יתרת נכסים")}}</th>
                    <th class="text-right" style="font-weight: bold;">{{trans("יתרה למיחזור")}}</th>

                    @if($request_type != \Config::get('constants.RECYCLING_LOAN'))
                    <th class="text-right" style="font-weight: bold;">{{trans("סכום הלוואה מירבי")}}</th>
                    @endif
                </tr>
                <?php
                $account_balance_sum = 0;
                $loan_repayment_sum = 0;
                foreach ($userAccounts as $userAccount):
                    //$account_balance_sum += $userAccount->account_balance * $userAccount->flag_user_accounts;
                    $account_balance_sum += $userAccount->account_balance;
                    $loan_repayment_sum += $userAccount->loan_repayment;

                    ?>
                    <tr>
                        <td><?=$userAccount->account_number?></td>
                        <td><?=$userAccount->tax_code?></td>
                        <td><?=$userAccount->fund_name?></td>
                        <td><?=number_format($userAccount->account_balance,1)?></td>
                        <td><?=number_format($userAccount->loan_repayment,1)?></td>

                        @if($request_type != \Config::get('constants.RECYCLING_LOAN'))
                        <td><?=number_format($userAccount->max_loan_new,1)?></td>
                        @endif
                    </tr>
                    <?php endforeach;?>

                <tr>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>
                    <td class="text-right" style="font-weight: bold;"><?=number_format($account_balance_sum,1)?></td>
                    <td class="text-right" style="font-weight: bold;"><?=number_format($loan_repayment_sum,1)?></td>

                    @if($request_type != \Config::get('constants.RECYCLING_LOAN'))
                    <td class="text-right" style="font-weight: bold;"><?=number_format($maxPossibleLoan,1)?></td>
                    @endif
                </tr>
            </table><br><br>
        </div>
    </div>



    <div class="row"> <!-- section for the graph -->
    </div><!-- end div class row -->
    <a name="stage"></a>


    <div calss="row">
        <div class="col-md-12">
            <div class="pricing-tables">
                <h3>{{trans("יש לבחור את המסלול המועדף עליך מתוך האפשרויות להלן")}}
                </h3>

            <?php if(in_array($typeTracks, array(0,1,3,4))){  ?>
                <div class="col-md-4 text-center borderltab mouse-o-c">
            <?php }else{ ?>
                <div style="display:none;" class="col-md-4 text-center borderltab mouse-o-c">
            <?php } ?>
                    <div>
                        <h2 class="r_h2">{{trans("מסלול א")}}</h2>
                        <div class="tsub_title">
                            <span class="readMore"><a href="#stage" style="font-size: 17px;" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="{{trans("מסלול זה ניתן בריבית המשתנה על פי השינויים בריבית הפריים . ריבית זו מבוססת על ריבית הבסיס אותה קובע בנק ישראל מדי חודש בתוספת קבועה של 1.5%. למשל: אם ריבית הבסיס נקבעת על 0.1%, ריבית הפריים תתעדכן ל-1.6%. את שיעור ריבית הבסיס ניתן למצוא באתר בנק ישראל. במסלול זה אין הצמדה למדד.")}}"><i class="fa fa-question-circle"></i></a></span>
                            {{trans("ריבית משתנה")}}
                        </div>

                        @if($risk_spread_message)
                            <div class="tvalue" style="color:red;" id="tvalue_float"><?=$floatInterest?>%</div>
                        @else
                            <div class="tvalue" id="tvalue_float"><?=$floatInterest?>%</div>
                        @endif

                        <input type="hidden" id="floatInt" name="floatInt" value="<?=$floatInterest?>">
                        <hr/>
                        <div style="margin-top: 24px; ">
                            @if($risk_spread_message)
                                <div class="tsub_title" style="height:97px;color:red;">
                                <label class="tsub_title">{{trans('תנאי מתן האשראי נקבעו בין היתר על יסוד דוח אשראי שהתקבל בגינך מהמרכז הישראלי לנתוני אשראי צרכני בהתאם לחוק שירות נתוני אשראי, תשס"ב-2002.')}}
                                </label>
                                </div>
                            @else
                            <div id="keren1" class="tsub_title">&nbsp;</div>
                            <div id="month_payment1" class="tsub_title">&nbsp;</div>
                            <div id="total_pay1" class="tsub_title">&nbsp;</div>
                            <div id="total_ribit1" class="tsub_title">&nbsp;</div>
                            @endif
                            <hr/>
                            <!--href="#stage"-->
                            <div><a id="sButt3" style="font-size: 17px;" class="pt_off" onclick="return showPaymentsTable('float')">{{trans("לוח סילוקין")}}</a></div>

                        <?php if(in_array($typeTracks, array(0,1,3,4))){  ?>
                            <div><br/>

                            <button style="font-size:17px;" class="btn green btn-block fa fa-angle-double-left" type="button" onclick="gotoSaveLoanDM('float')"> {{trans("בחר והמשך")}}</button>

                            <br/><br/></div>
                        <?php }?>
                        </div>
                    </div>
                </div>

            <?php if(in_array($typeTracks, array(0,4))){  ?>
                <div class="col-md-4 text-center borderltab mouse-o-c">
            <?php }else{ ?>
                <div style="display:none;" class="col-md-4 text-center borderltab mouse-o-c">
            <?php } ?>
                    <div>
                        <h2 class="r_h2">{{trans("מסלול משולב")}}</h2>
                        <div class="tsub_title">
                            <span class="readMore"><a href="#stage" style="font-size: 17px;" data-toggle="tooltip" data-placement="bottom" data-original-title="{{trans("מסלול זה מעניק שילוב שבין שני המסלולים, ובעצם מחלק את הקרן בין מסלול ריבית קבועה למסלול ריבית משתנה. כך הסיכון והחשיפה לשינוי הריבית ניתנת לשליטה על ידך")}}"><i class="fa fa-question-circle"></i></a></span>
                            {{trans("ריבית משולבת")}}
                        </div>

                        @if($risk_spread_message)
                            <div class="tvalue" style="color:red;" id="tvalue_mix"><?=$fixedInterest?>%</div>
                        @else
                            <div class="tvalue" id="tvalue_mix"><?=$fixedInterest?>%</div>
                        @endif

                        <hr/>
                        <div>
                            <div class="pull-left tsub_title">
                                {{trans("ריבית קבועה")}}
                            </div>
                            <div class="pull-right tsub_title">
                                {{trans("ריבית משתנה")}}
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="pull-left">
                                        <input readonly="1" type="text" id="slider_money_value1" class="form-control noborder" style="width:50px; background-color: transparent; border: none; -webkit-box-shadow: none; box-shadow: none; font-weight: normal; font-size: 17px;" value="">
                                    </div></div>

                                <div class="col-md-6">
                                    <div class="pull-right">
                                        <input readonly="1" type="text" id="slider_money_value2" class="form-control noborder" style="width:50px; background-color: transparent; border: none; -webkit-box-shadow: none; box-shadow: none; font-weight: normal; font-size: 17px;" value="">
                                    </div></div>
                            </div>


                            <div style="margin-top: -18px;">
                                <div id="slider_interest"></div>
                                <input type="hidden" id="slider_interest_value" value="1">
                            </div>

                            <div class="pull-left" id="mix_fixed">
                                <div id="mix_fix_left"></div>
                            </div>
                            <div class="pull-right" id="mix_prime">
                                <div id="mix_prime_right"></div>
                            </div>

                            <input type="hidden" id="mixedInt" name="fixedInt" value="">
                        </div>


                        <div style="margin-top: 51px;">
                            <hr/>
                            <!--div id="keren" class="tsub_title">&nbsp;</div>
                            <div id="month_payment" class="tsub_title">&nbsp;</div>
                            <div id="total_pay" class="tsub_title">&nbsp;</div>
                            <div id="total_ribit" class="tsub_title">&nbsp;</div-->
                            <!--href="#stage"-->
                            <a id="sButt2" style="font-size: 17px;" class="pt_off" onclick="return showPaymentsTable('mix')">{{trans("לוח סילוקין")}}</a>
                        <?php if(in_array($typeTracks, array(0,4))){  ?>
                            <div><br/>
                               <button style="font-size:17px;" class="btn green btn-block fa fa-angle-double-left" type="button" onclick="gotoSaveLoanDM('mix')"> {{trans("בחר והמשך")}}</button>
                            <br/><br/></div>
                        <?php } ?>
                        </div>
                    </div>
                </div>


            <?php if(in_array($typeTracks, array(0,2,3,4))){  ?>
                <div class="col-md-4 text-center borderltab mouse-o-c">
            <?php }else{ ?>
                <div style="display:none;" class="col-md-4 text-center borderltab mouse-o-c">
            <?php } ?>
                    <div>
                        <h2 class="r_h2">{{trans("מסלול ב")}}</h2>
                        <div class="tsub_title">
                            <span class="readMore"><a href="#stage" style="font-size: 17px;" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="{{trans("מסלול זה מעניק ביטחון מוחלט בהחזרי הריבית והקרן של ההלוואה לפי סוג ההחזר שנבחר. הריבית בו קבועה ולא משתנה לאורך ההלוואה ואין הצמדה למדד.")}}"><i class="fa fa-question-circle"></i></a></span>
                            {{trans("ריבית קבועה")}}
                        </div>
                        @if($risk_spread_message)
                            <div class="tvalue" style="color:red;" id="tvalue_fix"><?=$fixedInterest?>%</div>
                        @else
                            <div class="tvalue" id="tvalue_fix"><?=$fixedInterest?>%</div>
                        @endif

                        <input type="hidden" id="fixedInt" name="fixedInt" value="<?=$fixedInterest?>">
                        <hr/>
                        <div style="margin-top: 24px; ">
                            @if($risk_spread_message)
                                <div class="tsub_title" style="height:97px;color:red;">
                                <label class="tsub_title">{{trans('תנאי מתן האשראי נקבעו בין היתר על יסוד דוח אשראי שהתקבל בגינך מהמרכז הישראלי לנתוני אשראי צרכני בהתאם לחוק שירות נתוני אשראי, תשס"ב-2002.')}}
                                </label>
                                </div>
                            @else
                                <div id="keren2" class="tsub_title">&nbsp;</div>
                                <div id="month_payment2" class="tsub_title">&nbsp;</div>
                                <div id="total_pay2" class="tsub_title">&nbsp;</div>
                                <div id="total_ribit2" class="tsub_title">&nbsp;</div>
                            @endif
                            <hr/>
                            <!-- href="#stage" -->
                            <a id="sButt1" style="font-size: 17px;" class="pt_off" onclick="return showPaymentsTable('fix')">{{trans("לוח סילוקין")}}</a>
                        <?php if(in_array($typeTracks, array(0,2,3,4))){  ?>
                            <div><br/>
                              <button style="font-size:17px;" class="btn green btn-block fa fa-angle-double-left" type="button" onclick="gotoSaveLoanDM('fix')"> {{trans("בחר והמשך")}}</button>
                            <br/><br/></div>
                        <?php } ?>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <div class="row"> <!-- section for the graph -->
    </div><!-- end div class row -->

    <div class="row"> <!-- section for the graph -->
    </div><!-- end div class row -->
    <div id='summery' class='summery text-center'></div>
    <div id='interestOnlysummery' class='summery text-center' style="margin-top: 30px;">
        <div class='row'>
            <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>
                <h3 id="interestOnlyEndDate"></h3>
                <?=_lang('pay_off_date')?>
            </div>

            <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>
                <h3 id="interstOnlyTotalInterest"></h3>
                <?=_lang('total_int_paid')?>
            </div>

            <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>
                <h3 id="interstOnlyTotalPayment"></h3>
                <?=_lang('total_of')?> <span id="paymentCount"></span> <?=_lang('of_payments')?>
            </div>

            <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>
                <h3 id="interestOnlyMonthlyPayment"></h3>
                <?=_lang('monthly_principal')?>
            </div>
        </div>
    </div>
    <div id='maturitySummary' class='summery text-center' style="margin-top: 30px;">
        <div class='row'>
            <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>
                <h3 id="maturityEndDate"></h3>
                <?=_lang('pay_off_date')?>
            </div>

            <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>
                <h3 id="maturityTotalInterest"></h3>
                <?=_lang('total_int_paid')?>
            </div>

            <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>
                <h3 id="maturityTotalPayment"></h3>
                <?=_lang('total_of')?> <span id="paymentCount"></span> <?=_lang('of_payments')?>
            </div>

            <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>
                <h3 id="maturityMonthlyPayment"></h3>
                <?=_lang('monthly_principal')?>
            </div>
        </div>
    </div>
    <div style="margin-right: 15px; margin-top: 45px">
        <h3>*
            {{trans("לתשלום הראשון תתווסף עמלה תפעולית בסך")}}
            <?=number_format($userAccounts->get(0)->operation_fee,2)?>
            {{trans("₪")}}
        </h3>
    </div>
    <div id="paymentsCont" style="display: none"><a name="payments_table"></a>
        <div id='summery' class='summery text-center'></div>
        <div class='table-responsive' style="margin-top:20px;">
            <table id="schedule" class='table table-bordered table-striped table-hover' style="max-width: 1140px; margin-right: 15px;">
            </table>
        </div>

        <div class='table-responsive' style="margin-top:20px;">
            <table id="interestOnly" class='table table-bordered table-striped table-hover'>
                <thead>
                    <tr>
                        <td>{{trans("תאריך")}}</td>
                        <td>{{trans("תשלום")}}</td>
                        <td>{{trans("ריבית")}}</td>
                        <td>{{trans("קרן")}}</td>
                        <td>{{trans("יתרה")}}</td>
                    </tr>
                </thead>
                <tbody id="interestBody">
                </tbody>
                <tfoot id="interestFoot">
                    <tr>
                        <td>{{trans('סה"כ')}}</td>
                        <td>{{trans("₪")}}<span id="interestOnlyPaymentTotal"></span></td>
                        <td>{{trans("₪")}}<span id="interestOnlyTotal"></span></td>
                        <td>{{trans("₪")}}<span id="interestOnlyPrincipalTotal"></span></td>
                        <td><span id="interestOnlyBalanceTotal"></span></td>
                    </tr>
                </tfoot>
            </table>
        </div>

        <div class='table-responsive' style="margin-top:20px;">
            <table id="maturity" class='table table-bordered table-striped table-hover'>
                <thead>
                    <tr>
                        <td>{{trans("תאריך")}}</td>
                        <td>{{trans("תשלום")}}</td>
                        <td>{{trans("ריבית")}}</td>
                        <td>{{trans("קרן")}}</td>
                        <td>{{trans("יתרה")}}</td>
                    </tr>
                </thead>
                <tbody id="maturityBody">
                </tbody>
                <tfoot>
                    <tr>
                        <td>{{trans('סה"כ')}}</td>
                        <td>₪<span id="maturityPayment"></td>
                        <td>₪<span id="maturityInterest"></td>
                        <td>₪<span id="maturityPrincipal"></td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<!-- end row -->
</div>

<script src="/loancalc/js/jquery.1.10.2.min.js"></script>
<script src="/loancalc/js/bootstrap.min.js"></script>
<script src="/loancalc/js/underscore.min.js"></script>
<script src="/loancalc/js/nouislider.min.js"></script>
<script src="/loancalc/js/wnumb.js"></script>
<script>
    var gPricingTable = jQuery.parseJSON('<?=json_encode($loanPricing);?>');

    var sliderInterest = document.getElementById('slider_interest');

    var range = {
    'min': 0,
    '10%': 10,'15%': 15,'20%': 20,'25%': 25,'30%': 30,'35%': 35,
    '40%': 40,'45%': 45,'50%': 50,
    '55%': 55,'60%': 60,'65%': 65,'70%': 70,'75%': 75,
    '80%': 80,'85%': 85,'90%': 90,'95%': 95,'max': 100};

    noUiSlider.create(sliderInterest, {
        start: [ 0 ],
        range: range,
        direction: 'rtl',
        snap:true,
        format: wNumb({
            decimals: 0,
            thousand: ',',
            postfix: '%',

        })
    });

    var sliderMoney = document.getElementById('slider_money');

    noUiSlider.create(sliderMoney, {
        start: [<?=$maxPossibleLoan?>],
        range: {
            <?php
            $diff = ($maxPossibleLoan - $minPossibleLoan)/100;
            $sum = $minPossibleLoan + $diff;

            echo "'min': ".$minPossibleLoan.",";
            //for($i = 2; $i< 100; $i++){
            //echo "'$i%': $sum,";
            //$sum += $diff;
            //}
            echo "'max': $maxPossibleLoan";
            ?>
        },
        behaviour: 'tap',
        direction: '{{$dir_negative}}',
        snap:false,
        tooltips: true,
        format: wNumb({
            decimals: 0,
            thousand: ',',

        })
    });
    /* Linking the input field */
    var inputFormat_slider_money_value = document.getElementById('slider_money_value');
    sliderMoney.noUiSlider.on('update', function( values, handle ) {
        inputFormat_slider_money_value.value = values[handle];
        $('#slider_money_value').trigger( "keyup" );

        //Update sliderMoney
        var  num_slider = sliderInterest.noUiSlider.get();
        sliderInterest.noUiSlider.set(range[num_slider]);
    });

//    inputFormat_slider_money_value.addEventListener('blur', function() {
//        try{
//            var smvalue = $('#slider_money_value').val();
//
//            while(smvalue.indexOf(",") > -1)
//            smvalue = smvalue.replace(",","");
//
//            smvalue = parseInt(smvalue);
//
//            if (smvalue > <?=$maxPossibleLoan?>) {
//                $('#slider_money_value').val(<?=$maxPossibleLoan?>);
//                sliderMoney.noUiSlider.set($('#slider_money_value').val());
//            }
//            else if(smvalue <= <?=$maxPossibleLoan?> && smvalue >= <?=$minPossibleLoan?>) {
//                sliderMoney.noUiSlider.set(smvalue);
//            }
//            else {
//                sliderMoney.noUiSlider.set(<?=$minPossibleLoan?>);
//            }
//
//            $('#slider_money_value').trigger( "keyup" );
//        }
//        catch(err){}
//    });

    inputFormat_slider_money_value.addEventListener('keypress', function (e) {
        var key = e.which || e.keyCode;

        if (key === 13) { // 13 is enter
            try{
                var smvalue = $('#slider_money_value').val();

                while(smvalue.indexOf(",") > -1)
                smvalue = smvalue.replace(",","");

                smvalue = parseInt(smvalue);

                if (smvalue > <?=$maxPossibleLoan?>) {
                    $('#slider_money_value').val(<?=$maxPossibleLoan?>);
                    sliderMoney.noUiSlider.set($('#slider_money_value').val());
                }
                else if(smvalue <= <?=$maxPossibleLoan?> && smvalue >= <?=$minPossibleLoan?>) {
                    sliderMoney.noUiSlider.set(smvalue);
                }
                else {
                    sliderMoney.noUiSlider.set(<?=$minPossibleLoan?>);
                }

                $('#slider_money_value').trigger( "keyup" );
            }
            catch(err){}
            }
    });

    inputFormat_slider_money_value.addEventListener('focusout', function (e) {

        try{
            var smvalue = $('#slider_money_value').val();

            while(smvalue.indexOf(",") > -1)
            smvalue = smvalue.replace(",","");

            smvalue = parseInt(smvalue);

            if (smvalue > <?=$maxPossibleLoan?>) {
                $('#slider_money_value').val(<?=$maxPossibleLoan?>);
                sliderMoney.noUiSlider.set($('#slider_money_value').val());
                document.getElementById('slider_money_value').scrollIntoView(true);
            }
            else if(smvalue <= <?=$maxPossibleLoan?> && smvalue >= <?=$minPossibleLoan?>) {
                sliderMoney.noUiSlider.set(smvalue);
            }
            else {
                sliderMoney.noUiSlider.set(<?=$minPossibleLoan?>);
                document.getElementById('slider_money_value').scrollIntoView(true);
            }

            $('#slider_money_value').trigger( "keyup" );

        }
        catch(err){}

    });


    /*-------------*/
    var sliderTerms = document.getElementById('slider_terms');

    $("#slider_terms_value").attr({"max" : <?=$yrsmax?>, "min" : <?=$yrsmin?>});

    noUiSlider.create(sliderTerms, {
        start: [ <?=$yrsmax?> ],
        range: {
            'min': <?=$yrsmin?>,
            <?php
            $diff = 100/$yrscount;
            $sum = $diff;
            for($i = 0; $i < $yrscount; $i++){
                echo "'$sum%': $yrs[$i],";
                $sum += $diff;
            }
            echo "'max': $yrsmax";
            ?>

        },
        snap:true,
        behaviour: 'tap',
        direction: '{{$dir_negative}}',
        tooltips: true,
        format: wNumb({
            decimals: 0,
            /*postfix: 'שנים',  */
        })

    });
    /* Linking the input field */
    var inputFormat_slider_terms_value = document.getElementById('slider_terms_value');
    sliderTerms.noUiSlider.on('update', function( values, handle ) {
        inputFormat_slider_terms_value.value = values[handle];
        $( inputFormat_slider_terms_value ).trigger( "keyup" );
        setFixAndFloatInt($('#slider_terms_value').val());
    });


    function setFixAndFloatInt(y){
        var cureFloatInterest = 0;
        var cureFixInterest = 0;

        for(var i = 0; i < gPricingTable.length; i++){
            var price = gPricingTable[i];
            if(price.lq == 1 && price.years == y){

                if(price.interest_type_to_display == "קבועה")
                    cureFixInterest = price;
                else
                    cureFloatInterest = price;
            }
        }

        if(cureFloatInterest != 0){
            var typeVal =  $("#type").val();

            if(typeVal == "AmortizedPayments")
                $("#floatInt").val(parseFloat(cureFloatInterest.spitzer).toFixed(2));

            if(typeVal == "InterestOnlyPayments")
                $("#floatInt").val(parseFloat(cureFloatInterest.partial_balloon).toFixed(2));

            if(typeVal == "PrincipalandInterestatMaturity")
                $("#floatInt").val(parseFloat(cureFloatInterest.full_balloon).toFixed(2));

            $("#tvalue_float").html($("#floatInt").val() + "%");
        }

        if(cureFixInterest != 0){
            var typeVal =  $("#type").val();

            if(typeVal == "AmortizedPayments")
                $("#fixedInt").val(parseFloat(cureFixInterest.spitzer).toFixed(2));

            if(typeVal == "InterestOnlyPayments")
                $("#fixedInt").val(parseFloat(cureFixInterest.partial_balloon).toFixed(2));

            if(typeVal == "PrincipalandInterestatMaturity")
            $("#fixedInt").val(parseFloat(cureFixInterest.full_balloon).toFixed(2));

            $("#tvalue_fix").html($("#fixedInt").val() + "%");
        }


        try{
            sliderInterest.noUiSlider.set(sliderInterest.noUiSlider.get());
        }
        catch(err){}
    }

    function isValidYear(y){
        <?php
        for($i = 0; $i < $yrscount; $i++){
            echo "if(y == $yrs[$i])return true; ";
        }
        ?>
        return false;
    }


//    inputFormat_slider_terms_value.addEventListener('keypress', function(){
//        inputFormat_slider_terms_value.addEventListener('keyup', function() {
//            if (!isValidYear($('#slider_terms_value').val())) {
//                //$('#slider_terms_value').val(<?=$yrs[0]?>);
//                $('#slider_terms_value').val(this.value);
//                sliderTerms.noUiSlider.set($('#slider_terms_value').val());
//            }
//            else if(isValidYear($('#slider_terms_value').val())) {
//                sliderTerms.noUiSlider.set(this.value);
//            }
//            else {
//                sliderTerms.noUiSlider.set(null);
//            }
//
//            setFixAndFloatInt($('#slider_terms_value').val());
//        });
//    });

    inputFormat_slider_terms_value.addEventListener('keypress', function(eventObject){

        eventObject.preventDefault();
        sliderTerms.noUiSlider.set(String.fromCharCode(eventObject.which));

        setFixAndFloatInt($('#slider_terms_value').val());
    });

    inputFormat_slider_terms_value.addEventListener('focusout', function(eventObject){

        eventObject.preventDefault();
        sliderTerms.noUiSlider.set(String.fromCharCode(eventObject.which));

        setFixAndFloatInt($('#slider_terms_value').val());
    });
    /*-----------*/

    /* Linking the input field */
    var inputFormat_slider_interest_value = document.getElementById('slider_interest_value');
    var rangeTermElement = document.getElementById('slider_terms_value');
    var loan_amount = document.getElementById('slider_money_value');
    var rangeMoneyValueElement = document.getElementById('slider_money_value1');
    var rangeMoneyValueElement2 = document.getElementById('slider_money_value2');
    var mix_fix_left = document.getElementById('mix_fix_left');
    var mix_prime_right = document.getElementById('mix_prime_right');

    sliderInterest.noUiSlider.on('update', function( values, handle ) {
        rangeMoneyValueElement.value = values[handle];
        loanAmount = loan_amount.value;
        loanAmount = loanAmount.replace(/\,/g, '');

        rangeMoneyValueElement2.value = 100 - parseFloat(values[handle]) + '%';
        fix_left = (parseInt(values[handle]) / 100) * loanAmount;
        mix_fix_left.innerHTML = '₪' + fix_left.toFixed(2);

        prime_right  = ((parseFloat(rangeMoneyValueElement2.value)) / 100) * loanAmount;
        mix_prime_right.innerHTML = '₪' + prime_right.toFixed(2);

        var LeftPercent = parseFloat($('#slider_money_value1').val()) / 100;
        var RightPercent = parseFloat($('#slider_money_value2').val()) / 100;
        var fixed_Int = parseFloat($('#fixedInt').val());
        var float_Int = parseFloat($('#floatInt').val());
        var mixInterest = (parseFloat(LeftPercent) * parseFloat(fixed_Int)) + (parseFloat(RightPercent) * parseFloat(float_Int));

        if (rangeMoneyValueElement.value==0){
            $('.pricing-table-content #mixInt').text(LeftPercent + '%');
        }
        if (rangeMoneyValueElement2.value==0){
            $('.pricing-table-content #mixInt').text(RightPercent + '%');
        } else {
            $('.pricing-table-content #mixInt').text(mixInterest.toFixed(2) + '%');
        }

        $("#tvalue_mix").text(parseFloat(mixInterest).toFixed(2) + "%");
        $("#mixedInt").val(mixInterest);

        $(inputFormat_slider_interest_value).val(mixInterest);
        $('#slider_money_value').trigger( "keyup" );
    });

</script>
<script src="/loancalc/js/financejs/finance.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script>
    var months = ["<?=_lang('jan')?>", "<?=_lang('feb')?>", "<?=_lang('mar')?>", "<?=_lang('apr')?>", "<?=_lang('may')?>", "<?=_lang('jun')?>", "<?=_lang('jul')?>", "<?=_lang('aug')?>", "<?=_lang('sep')?>", "<?=_lang('oct')?>", "<?=_lang('nov')?>", "<?=_lang('dec')?>"];
</script>
<script src="/loancalc/js/app.js"> </script>
<script src="/loancalc/js/graph-hightcharts.js"> </script>
<script type="text/template" id="summery-template">
    <div class='row'>
    <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>
    <h3><%- rc.payoffDate %> </h3>
    <?=_lang('pay_off_date')?>
    </div>

    <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>
    <h3 id='s_totalInterest'><%- finance.format(rc.totalInterest, '<?=_lang('currency')?>') %> </h3>
    <?=_lang('total_int_paid')?>
    </div>

    <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>
    <h3 id='s_totalPayments'><%- finance.format(rc.totalPayments, '<?=_lang('currency')?>') %> </h3>
    <?=_lang('total_of')?> <%- rc.totalPaymentsNum %> <?=_lang('of_payments')?>
    </div>

    <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>
    <h3 id='s_monthlyPayment'><%- finance.format(rc.monthlyPayment, '<?=_lang('currency')?>') %> </h3>
    <?=_lang('monthly_principal')?>
    </div>



    </div>
</script>
<script type="text/template" id="table-template">
    <thead>
    <tr>
    <td><?=_lang('date')?></td>
    <td><?=_lang('payment')?></td>
    <td><?=_lang('interest')?></td>
    <td><?=_lang('principle')?></td>
    <td><?=_lang('balance')?></td>
    </tr>
    </thead>
    <tbody>
    <tr class="getStartingBalance">
    <td><span id="amortDate1"></span></td>
    <td>₪<?=number_format($userAccounts->get(0)->operation_fee,2)?></td>
    <td colspan="2"><span id="startingBalance1"></span></td>
    <td><span id="amortBalance1"></span></td>
    </tr>
    <tr class="getStartingBalance">
    <td><span id="amortDate2"></span></td>
    <td colspan="3"><span id="startingBalance2"></span></td>
    <td><span id="amortBalance2"></span></td>
    </tr>
    <% _.each( rc.items, function( item ){ %>
        <tr>
        <td> <%- item.date %> </td>
        <td><%- finance.format(item.payment, '<?=_lang('currency')?>')  %></td>
        <td><%- finance.format(item.paymentToInterest, '<?=_lang('currency')?>') %></td>
        <td><%- finance.format(item.paymentToPrinciple, '<?=_lang('currency')?>') %></td>
        <td><%- finance.format(item.principle, '<?=_lang('currency')?>') %></td>
        </tr>
        <% }); %>
    </tbody>
    <tfoot>
    <tr>
    <td>{{trans('סה"כ')}}</td>
    <td><%- finance.format(rc.totalPayments, '<?=_lang('currency')?>') %></td>
    <td><%- finance.format(rc.totalInterest, '<?=_lang('currency')?>') %></td>
    <td><%- finance.format(rc.totalPrinciple, '<?=_lang('currency')?>') %></td>
    <td>&nbsp;</td>
    </tr>
    </tfoot>
</script>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $("#type").change(function(){
            $(this).find("option:selected").each(function(){
                if($(this).attr("value")=="AmortizedPayments"){
                    $("#maturity").hide();
                    $("#interestOnly").hide();
                    $("#interestOnlysummery").hide();
                    $("#maturitySummary").hide();
                    $("#schedule").show();
                    $("#summery").show();

                    //$('#months').prop('disabled', false);
                }
                else if($(this).attr("value")=="InterestOnlyPayments"){
                    $("#interestOnly").show();
                    $("#maturity").hide();
                    $("#maturitySummary").hide();
                    $("#interestOnlysummery").show();
                    $("#schedule").hide();
                    $("#summery").hide();

                    //$('#months').prop('disabled', false);
                }
                else if($(this).attr("value")=="PrincipalandInterestatMaturity"){
                    $("#interestOnly").hide();
                    $("#schedule").hide();
                    $("#interestOnlysummery").hide();
                    $("#maturitySummary").show();
                    $("#summery").hide();
                    $("#maturity").show();

                    $('#months option[value=<?=(date("n") - 1)?>]').prop('selected','selected');
                    //$('#months').prop('disabled', 'disabled');
                }
                else{
                    $(".box").hide();
                }

                try{
                    setFixAndFloatInt($('#slider_terms_value').val());
                    $("#paymentsCont").css("display", "block");
                }
                catch(err){}
            });
        }).change();

        //First load

        <?php if(in_array($typeTracks, array(1,3,4,0))){  ?>
        showPaymentsTable('float');
        $('#schedule').html('');
        <?php } ?>

        <?php if(in_array($typeTracks, array(2))){  ?>
        showPaymentsTable('fix');
        $('#schedule').html('');
        <?php } ?>

    });


    function gotoSaveLoan(type){

        showPaymentsTable(type);

        var typeVal =  $("#type").val();
        var p_interest;
        var p_total_interestpaid;
        var p_monthly_payment;
        var p_total_payment;

        if(typeVal == "AmortizedPayments"){
            p_total_interestpaid = $("#s_totalInterest").html();
            p_monthly_payment = $("#s_monthlyPayment").html();
            p_total_payment = $("#s_totalPayments").html();
        }

        if(typeVal == "InterestOnlyPayments"){
            p_total_interestpaid = $("#interstOnlyTotalInterest").html();
            p_monthly_payment = $("#interestOnlyMonthlyPayment").html();
            p_total_payment = $("#interstOnlyTotalPayment").html();
        }

        if(typeVal == "PrincipalandInterestatMaturity"){
            p_total_interestpaid = $("#maturityTotalInterest").html();
            p_monthly_payment = "0";
            p_total_payment = $("#maturityTotalPayment").html();
        }

        $("#p_type").val(typeVal);

        if(type == "fix")
            p_interest = $("#fixedInt").val();

        if(type == "float")
            p_interest = $("#floatInt").val();

        if(type == "mix")
            p_interest = $("#mixedInt").val();

        p_total_interestpaid = p_total_interestpaid.replace("₪","").replace(",","");
        p_monthly_payment = p_monthly_payment.replace("₪","").replace(",","");
        p_total_payment = p_total_payment.replace("₪","").replace(",","");

        $("#p_interest").val(p_interest);
        $("#p_interest_type").val(type);
        $("#p_loan_amount").val($("#slider_money_value").val());
        $("#p_total_interestpaid").val(p_total_interestpaid);
        $("#p_monthly_payment").val(p_monthly_payment);
        $("#p_total_payment").val(p_total_payment);
        $("#p_years").val($("#slider_terms_value").val());

        if(typeVal != "PrincipalandInterestatMaturity"){
            $("#p_start_month").val($("#months").val());
            $("#p_start_year").val($("#months").find(":selected").attr("y"));
        }
        else{
            $("#p_start_month").val($("#months").val());
            $("#p_start_year").val($("#months").find(":selected").attr("y"))
        }

        $("#p_interest_float_percent").val($("#slider_money_value1").val().replace("%",""));
        $("#p_interest_mix_percent").val($("#slider_money_value2").val().replace("%",""));

        $("#act_form").submit();
    }

    function showPaymentsTable(type){
        var val = 0;

        if(type == "fix"){
            val = $("#fixedInt").val();
            <?php if(!in_array($typeTracks, array(4))){  ?>
            sliderInterest.noUiSlider.set(100);
            <?php } ?>
        }

        if(type == "float"){
            val = $("#floatInt").val();
            <?php if(!in_array($typeTracks, array(4))){  ?>
            sliderInterest.noUiSlider.set(0);
            <?php } ?>
        }

        if(type == "mix")
            val = $("#mixedInt").val();

        $(inputFormat_slider_interest_value).val(val);
        $(inputFormat_slider_money_value).trigger( "keyup" );

        $("#paymentsCont").css("display", "block");

        return true;
    }


</script>
<script src="/loancalc/js/sweetalert.min.js"></script>
<script>
    function gotoSaveLoanDM(type){

        //Spinner on
        swal({
            title: "{{trans('Please wait')}}",
            imageUrl: "../gif/snake.gif",
            showConfirmButton: false,
            allowOutsideClick: false
        });

        //Send ajax check checkIncomeStatement
        $.post( "/loans/ajax/checkIncomeStatement", { monthly_prepayment: $('#s_monthlyPayment').text(), type: $('#type').val() })
            .done(function( data ) {

                if(data.result == 1){
                    //Send ajax check setup
                    $.post( "/loans/ajax/checkSetup", { money_value: $('#slider_money_value').val()})
                        .done(function( data ) {

                            if(data.result == 1){
                                comfirmedChoise(type);
                            }else{
                                preSaveError(data.delta, data.brutto, data.account_number, data.fund_name);
                            }
                        });
                }else{
                    IncomeStatementError(data.delta);
                }
            });

    }

    function comfirmedChoise(type){
        var typeVal =  $("#type").val();
        var swal_title = "";
        var g_dm_type;

        g_dm_type = type;

        if(type == "fix"){
            swal_title = "{{trans("בחרת במסלול ב בריבית קבועה")}}";
        }
        else if(type == "mix"){
            swal_title = "{{trans("בחרת במסלול משולב")}}";
        }
        else if(type == "float"){
            swal_title = "{{trans("בחרת במסלול א בריבית משתנה")}}";
        }

        swal({
                title: swal_title,
                text: "{{trans("האם להמשיך?")}}",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "{{trans("אשר")}}",
                cancelButtonText: "{{trans("בטל")}}",
                closeOnConfirm: true,
                closeOnCancel: true },
            function(isConfirm){
                if (isConfirm) {
                    gotoSaveLoan(g_dm_type);
                } else {

                }
            });
    }

    function preSaveError(delta, brutto, account_number, fund_name){
        swal({
            title: "<div style='color:red;'>{{trans("סכום הלוואה לחשבון")}} <div style='text-decoration:underline; color:black;'>" + account_number + " " + fund_name + "</div> {{trans("נמוך מהנדרש")}}</div>",
            text: "<div'>{{trans("text_error_part1")}} " + '<div style="text-decoration:underline; font-weight: bold;">' +finance.format(delta, '<?=_lang('currency')?>') + "</div>" + '{{trans("text_error_part2")}}</div>' + '<div style="text-decoration:underline; font-weight: bold;">'+finance.format(brutto, '<?=_lang('currency')?>')+ '</div>' + ' {{trans("text_error_part3")}}',
            html: true
        });

    }

    function IncomeStatementError(delta){
        swal({
            title: "<div style='color:red;'>{{trans("סכום החזר חודשי גדול מהמותר.")}}</div>",
            text: "<div>{{trans("ש לערוך בקשת הלוואה מחדש כך שסכום ההחזר החודשי יהיה קטן מ")}} " + finance.format(delta, '<?=_lang('currency')?>') + '</div>',
            html: true
        });

    }
</script>
@endsection
